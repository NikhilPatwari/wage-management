package com.example.nikhil.wageManagement;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class WageManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(WageManagementApplication.class, args);
	}
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/employee").allowedOrigins("http://localhost:63342");
				registry.addMapping("/tasks").allowedOrigins("http://localhost:63342");
			}
		};
	}
}
