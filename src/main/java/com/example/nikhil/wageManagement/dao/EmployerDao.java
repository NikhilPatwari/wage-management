package com.example.nikhil.wageManagement.dao;

import com.example.nikhil.wageManagement.model.Employer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EmployerDao extends JpaRepository<Employer,Integer> {
    public Optional<Employer> findByName(String name);
}
