package com.example.nikhil.wageManagement.dao;

import com.example.nikhil.wageManagement.model.Employee;
import com.example.nikhil.wageManagement.model.Tasks;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TasksDao extends JpaRepository<Tasks,Integer> {
    public List<Tasks> findByTaskName(String taskName);
}
