package com.example.nikhil.wageManagement.dao;

import com.example.nikhil.wageManagement.model.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttendanceDao extends JpaRepository<Attendance,Integer> {
}
