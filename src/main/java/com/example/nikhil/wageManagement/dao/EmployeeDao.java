package com.example.nikhil.wageManagement.dao;

import com.example.nikhil.wageManagement.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeDao extends JpaRepository<Employee,Integer> {
    public List<Employee> findByName(String name);
}
