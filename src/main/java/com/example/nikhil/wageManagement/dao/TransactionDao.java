package com.example.nikhil.wageManagement.dao;

import com.example.nikhil.wageManagement.model.Employee;
import com.example.nikhil.wageManagement.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface TransactionDao extends JpaRepository<Transaction,Integer> {
    public List<Transaction> getTransactionByEmployee(Employee employee);
}
