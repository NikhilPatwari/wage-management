
function getData(){
    let request = new XMLHttpRequest();
    request.open('GET','http://127.0.0.1:8080/employee');
    request.send();
    request.onload = function () {
        if(request.status === 200){
            let data = JSON.parse(request.response)
            data.forEach((employee) => {
                console.log(employee.name)
            })
        }else{
            console.log(`error ${request.status} ${request.statusText}`);
        }
    }
}
function getTasks()
{
    let table = document.getElementById('taskTable');
    if(table === null)
        console.log('table id not fetched');
    let request = new XMLHttpRequest();
    request.open('GET','http://127.0.0.1:8080/tasks');
    request.send();
    request.onload = function () {
        if (request.status === 200) {
            let data = JSON.parse(request.response);
            data.forEach((task) => {
                console.log(task.taskName)
            })
            data.forEach((task) => {
                let row = table.insertRow(-1);
                let cell1 = row.insertCell(0);
                let text1 = document.createTextNode(task.task_id);
                cell1.appendChild(text1);
                let cell2 = row.insertCell(1);
                let text2 = document.createTextNode(task.taskName);
                cell2.appendChild(text2);
                let cell3 = row.insertCell(2);
                let text3 = document.createTextNode(task.locationName);
                cell3.appendChild(text3);
                let cell4 = row.insertCell(3);
                let text4 = document.createTextNode(task.startDate);
                cell4.appendChild(text4);
                let cell5 = row.insertCell(4);
                let text5 = document.createTextNode(task.endDate);
                cell5.appendChild(text5);
                let cell6 = row.insertCell(5);
                let text6 = document.createTextNode(task.amount);
                cell6.appendChild(text6);
            })
        } else {
            console.log(`error ${request.status} ${request.statusText}`);
        }
    }
}
getTasks();
// let table = document.querySelector("taskTable");

// document.onload = getTasks()
