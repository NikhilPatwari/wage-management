package com.example.nikhil.wageManagement.Controller;

import com.example.nikhil.wageManagement.dao.TasksDao;
import com.example.nikhil.wageManagement.model.Tasks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class TaskController {
    @Autowired
    TasksDao tasksRepo;

    @GetMapping("/tasks")
    public List<Tasks> getTasks()
    {
        return tasksRepo.findAll();
    }
    @GetMapping("/tasks/id/{id}")
    public Optional<Tasks> getTasksById(@PathVariable int id)
    {
        return tasksRepo.findById(id);
    }
    @GetMapping("/tasks/name/{name}")
    public List<Tasks> getTasksByName(@PathVariable String name)
    {
        return tasksRepo.findByTaskName(name);
    }
    @PostMapping("/tasks/")

    public String addTasks(@RequestBody Tasks tasks)
    {
        tasksRepo.save(tasks);
        return "Success";
    }
}
