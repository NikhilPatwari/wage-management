package com.example.nikhil.wageManagement.Controller;

import com.example.nikhil.wageManagement.dao.EmployeeDao;
import com.example.nikhil.wageManagement.dao.TransactionDao;
import com.example.nikhil.wageManagement.model.Employee;
import com.example.nikhil.wageManagement.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class TransactionController {
    @Autowired
    TransactionDao transactionrepo;
    @Autowired
    EmployeeDao employeeRepo;

    @GetMapping("/transaction/{employee_id}")
    public List<Transaction> getTransactionsByEmployeeId(@PathVariable int number,@PathVariable int employee_id)
    {
        Optional<Employee> emp = employeeRepo.findById(employee_id);
        Employee employee = emp.get();
        if(emp.isPresent())
        {
            return transactionrepo.getTransactionByEmployee(employee);
        }else
        {
            System.out.println("employee do not exist");
            List<Transaction>list = new ArrayList<>();
            list.add(new Transaction());
            return list;
        }
    }
    @GetMapping("transaction")
    public List<Transaction> getAllTransaction(){
        return transactionrepo.findAll();
    }
}
