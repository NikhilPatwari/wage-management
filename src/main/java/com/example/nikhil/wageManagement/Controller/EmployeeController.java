package com.example.nikhil.wageManagement.Controller;

import com.example.nikhil.wageManagement.dao.*;
import com.example.nikhil.wageManagement.model.Employee;
import com.example.nikhil.wageManagement.model.Employer;
import com.example.nikhil.wageManagement.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
public class EmployeeController {
    @Autowired
    AttendanceDao attendanceRepo;
    @Autowired
    EmployeeDao employeeRepo;
    @Autowired
    EmployerDao employerRepo;
    @Autowired
    TasksDao tasksRepo;
    @Autowired
    TransactionDao transactionRepo;

    // get all employee
    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/employee")
    public List<Employee> getAllEmployee()
    {
        return employeeRepo.findAll();
    }
    @GetMapping("/employee/name/{name}")
    public List<Employee>getEmployeeByName(@PathVariable("name") String name)
    {
        return employeeRepo.findByName(name);
    }
    @GetMapping("/employee/id/{id}")
    public Optional<Employee> getEmployeeByName(@PathVariable("id") int id)
    {
        return employeeRepo.findById(id);
    }
    // Api create an employee
    @PostMapping(path = "/employee",consumes = "application/json")
    public Employee addEmployee(@RequestBody Employee emp)
    {
        employeeRepo.save(emp);
        return emp;
    }

    @DeleteMapping(path = "/employee/{employee_id}")
    public String deleteById(@PathVariable int employee_id)
    {
        Optional<Employee> employee=employeeRepo.findById(employee_id);
        employeeRepo.deleteById(employee_id);
        return "deleted";
    }
}












