package com.example.nikhil.wageManagement.Controller;

import com.example.nikhil.wageManagement.dao.EmployeeDao;
import com.example.nikhil.wageManagement.dao.EmployerDao;
import com.example.nikhil.wageManagement.dao.TransactionDao;
import com.example.nikhil.wageManagement.model.Employee;
import com.example.nikhil.wageManagement.model.Employer;
import com.example.nikhil.wageManagement.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Optional;

@RestController
public class PaymentController {
    @Autowired
    EmployeeDao employeeRepo;
    @Autowired
    EmployerDao employerRepo;
    @Autowired
    TransactionDao transactionRepo;

    @PostMapping("/giveloan/{employee_id}/{employer_id}/{amount}")
    public String giveLoan(@PathVariable int employee_id, @PathVariable int employer_id, @PathVariable double amount)
    {
        LocalDate date = LocalDate.now();
        // first we create a transaction;
        Optional<Employee> emp = employeeRepo.findById(employee_id);
        Optional<Employer>empl = employerRepo.findById(employer_id);
        if(emp.isPresent()&&empl.isPresent())
        {
            //first transaction is created
            transactionRepo.save(new Transaction(date,amount*-1,"loan",emp.get(),empl.get()));
            // then we update the balance
            Employee employee = emp.get();
            employee.setBalance(employee.getBalance()-amount);
            employeeRepo.save(employee);
            return "Success";
        }else
            return "Failure -- either employee id or employer id does not exist.";
    }
    @PostMapping("/paysalary/{employee_id}/{employer_id}/{amount}")
    public String paySalary(@PathVariable int employee_id, @PathVariable int employer_id, @PathVariable double amount)
    {
        LocalDate date = LocalDate.now();
        // first we create a transaction;
        Optional<Employee> emp = employeeRepo.findById(employee_id);
        Optional<Employer>empl = employerRepo.findById(employer_id);
        if(emp.isPresent()&&empl.isPresent())
        {
            //first transaction is created
            transactionRepo.save(new Transaction(date,amount*-1,"Salary",emp.get(),empl.get()));
            // then we update the balance
            Employee employee = emp.get();
            employee.setBalance(employee.getBalance()-amount);
            employeeRepo.save(employee);
            return "Success";
        }else
            return "Failure -- either employee id or employer id does not exist.";
    }
}
