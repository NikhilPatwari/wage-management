package com.example.nikhil.wageManagement.Controller;

import com.example.nikhil.wageManagement.dao.AttendanceDao;
import com.example.nikhil.wageManagement.dao.EmployeeDao;
import com.example.nikhil.wageManagement.dao.EmployerDao;
import com.example.nikhil.wageManagement.dao.TasksDao;
import com.example.nikhil.wageManagement.model.Attendance;
import com.example.nikhil.wageManagement.model.Employee;
import com.example.nikhil.wageManagement.model.Employer;
import com.example.nikhil.wageManagement.model.Tasks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Optional;


@RestController
public class AttendanceController {
    @Autowired
    TasksDao tasksrepo;
    @Autowired
    EmployerDao employerRepo;
    @Autowired
    EmployeeDao employeeRepo;
    @Autowired
    AttendanceDao attendanceRepo;

    @PostMapping("/attendance/{employee_id}/{task_id}/{employer_id}/{multiplier}")
    public String markAttendance(@PathVariable int employee_id,@PathVariable int task_id,@PathVariable int employer_id,@PathVariable double multiplier)
    {
        // first an entry is saved to attendance
        // then amount is updated in the employee.amount
        Optional<Employee> employee = employeeRepo.findById(employee_id);
        Optional<Employer> employer = employerRepo.findById(employer_id);
        Optional<Tasks> tasks = tasksrepo.findById(task_id);
        if(employee.isPresent()&& tasks.isPresent()&&employer.isPresent())
        {
            Attendance attendance = new Attendance(multiplier, LocalDate.now(),tasks.get(),employee.get(),employer.get());
            attendanceRepo.save(attendance);
            // updating balance for employee
            Employee employee1 = employee.get();
            employee1.setBalance(employee1.getBalance()+(multiplier*tasks.get().getAmount()));
            employeeRepo.save(employee1);

            return "Success";
        }else
            return "Employer id or Employee id or Task Id is wrong";
    }

}
