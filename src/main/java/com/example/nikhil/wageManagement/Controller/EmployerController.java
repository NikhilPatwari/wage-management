package com.example.nikhil.wageManagement.Controller;

import com.example.nikhil.wageManagement.dao.EmployerDao;
import com.example.nikhil.wageManagement.model.Employer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class EmployerController {

    @Autowired
    EmployerDao employerRepo;

    @GetMapping("/employer")
    public List<Employer> getEmployer()
    {
        return employerRepo.findAll();
    }
    @GetMapping("employer/id/{employer_id}")
    public Optional<Employer> getEmployerById(@PathVariable int employer_id)
    {
        return employerRepo.findById(employer_id);
    }
    @GetMapping("employer/name/{name}")
    public Optional<Employer> getEmployerByName(@PathVariable String name)
    {
        return employerRepo.findByName(name);
    }
    @PostMapping("/employer")
    public Employer addEmployer(@RequestBody Employer employer)
    {
        employerRepo.save(employer);
        return employer;
    }
}
