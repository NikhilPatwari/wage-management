package com.example.nikhil.wageManagement.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "attendance")
public class Attendance {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int attendance_id;
    private double multiplier;
    private LocalDate dateTime;

    @ManyToOne
    @JoinColumn(name ="task_id", referencedColumnName = "task_id")
    private Tasks tasks;

    @ManyToOne
    @JoinColumn(name ="employee_id", referencedColumnName = "employee_id")
    private Employee employee;

    @ManyToOne
    @JoinColumn(name ="employer_id", referencedColumnName = "employer_id")
    private Employer employer;

    public int getAttendance_id() {
        return attendance_id;
    }

    public void setAttendance_id(int attendance_id) {
        this.attendance_id = attendance_id;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }

    public LocalDate getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDate dateTime) {
        this.dateTime = dateTime;
    }

    public Tasks getTasks() {
        return tasks;
    }

    public void setTasks(Tasks tasks) {
        this.tasks = tasks;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "Attendance{" +
                "attendance_id=" + attendance_id +
                ", multiplier=" + multiplier +
                ", dateTime=" + dateTime +
                ", tasks=" + tasks +
                ", employee=" + employee +
                ", employer=" + employer +
                '}';
    }

    public Attendance() {
    }

    public Attendance(double multiplier, LocalDate dateTime, Tasks tasks, Employee employee, Employer employer) {
        this.multiplier = multiplier;
        this.dateTime = dateTime;
        this.tasks = tasks;
        this.employee = employee;
        this.employer = employer;
    }
}
