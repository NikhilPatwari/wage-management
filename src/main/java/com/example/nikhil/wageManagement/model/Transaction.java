package com.example.nikhil.wageManagement.model;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name = "transaction")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int transaction_id;
    private LocalDate dateTime;
    private double amount;
    private String message;

    @ManyToOne
    @JoinColumn(name ="employee_id", referencedColumnName = "employee_id")
    private Employee employee;

    @ManyToOne
    @JoinColumn(name ="employer_id", referencedColumnName = "employer_id")
    private Employer employer;

    public int getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(int transaction_id) {
        this.transaction_id = transaction_id;
    }

    public LocalDate getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDate dateTime) {
        this.dateTime = dateTime;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public Transaction() {
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "transaction_id=" + transaction_id +
                ", dateTime=" + dateTime +
                ", amount=" + amount +
                ", message='" + message + '\'' +
                ", employee=" + employee +
                ", employer=" + employer +
                '}';
    }

    public Transaction(LocalDate dateTime, double amount, String message, Employee employee, Employer employer) {
        this.dateTime = dateTime;
        this.amount = amount;
        this.message = message;
        this.employee = employee;
        this.employer = employer;
    }
}
