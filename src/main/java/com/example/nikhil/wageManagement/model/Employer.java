package com.example.nikhil.wageManagement.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Employer {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int employer_id;
    private String name;


    public int getEmployer_id() {
        return employer_id;
    }

    public void setEmployer_id(int employer_id) {
        this.employer_id = employer_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Employer{" +
                "employer_id=" + employer_id +
                ", name='" + name + '\'' +
                '}';
    }
}
