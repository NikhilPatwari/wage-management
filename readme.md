## Project Description
In agriculture farm workers works on daily wages, monthly salary or on contract basis. They often take loan and supplies from their employer.
The farm work is also seasonal. Most of the employees are illetrate therefore the farmer himself has to manage their employee records.
As the scale increases it is difficult to manage records on pen and paper.So this project is about management of labour's salary/expenses specific to business of agriculture.

## User Stories
The following user stories are to be implemented with priority given to lower number.
---
1.	Farmer can create and delete account for employee.
2.	Farmer can create a task with compensation type and compensation value.
3.	Farmer can assign tasks to employee.
4.	Farmer can mark attendance of employee.
5.	Farmer can pay salary/loan to a employee.
6.	Farmer can get details of employee.
---
### API implemented 
The following API has been implemented till now
---
1. Get all employee using GET "/employee"
2. Get employee by id using GET "/employee/id/<employee_id>"
3. Get employee by name using Get "/employee/name/<employee_name>"
4. Create new employee by using POST "/employee"